import ConsumerSaleRegDraftSaleSimpleLineItem from './consumerSaleRegDraftSaleSimpleLineItem';
import ConsumerSaleRegDraftSaleCompositeLineItem from './consumerSaleRegDraftSaleCompositeLineItem';
import ConsumerPostalAddress from './consumerPostalAddress';

/**
 * @class {UpdateConsumerSaleRegDraftReq}
 */
export default class UpdateConsumerSaleRegDraftReq{

    _id:string;

    _firstName:string;

    _lastName:string;

    _address:ConsumerPostalAddress;

    _phoneNumber:string;

    _personEmail:string;

    _invoiceNumber:string;

    _sellDate:string;

    _partnerRepUserId:string;

    _partnerAccountId:string;

    _invoiceUrl:string;

    _isSubmitted:boolean;

    _simpleLineItems:ConsumerSaleRegDraftSaleSimpleLineItem[] ;

    _compositeLineItems:ConsumerSaleRegDraftSaleCompositeLineItem[];

    _createDate:string;

    constructor(
        id:string,
        firstName:string,
        lastName:string,
        address:ConsumerPostalAddress,
        phoneNumber:string,
        personEmail:string,
        sellDate:string,
        invoiceNumber:string,
        partnerAccountId:string,
        partnerRepUserId:string,
        invoiceUrl:string,
        isSubmitted:boolean,
        simpleLineItems:ConsumerSaleRegDraftSaleSimpleLineItem[],
        compositeLineItems:ConsumerSaleRegDraftSaleCompositeLineItem[],
        createDate:string
    ){

        if(!id){
            throw new TypeError('id required');
        }
        this._id = id;

        if(!firstName){
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if(!lastName){
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        if(!address){
            throw new TypeError('address required');
        }
        this._address = address;

        if(!phoneNumber){
            throw new TypeError('phoneNumber required');
        }
        this._phoneNumber = phoneNumber;

        this._personEmail = personEmail;

        if(!sellDate){
            throw new TypeError('sellDate required');
        }
        this._sellDate = sellDate;

        if(!invoiceNumber){
            throw new TypeError('invoiceNumber required');
        }
        this._invoiceNumber = invoiceNumber;

        if(!partnerAccountId){
            throw new TypeError('partnerAccountId required');
        }
        this._partnerAccountId = partnerAccountId;

        this._partnerRepUserId = partnerRepUserId;

        this._invoiceUrl = invoiceUrl;

        this._isSubmitted = isSubmitted;

        this._simpleLineItems = simpleLineItems;

        this._compositeLineItems = compositeLineItems;

        this._createDate = createDate;

    }

    /**
     * getter methods
     */

    get id():string{
        return this._id;
    }

    get firstName():string{
        return this._firstName;
    }

    get lastName():string{
        return this._lastName;
    }

    /**
     * @returns {ConsumerPostalAddress}
     */
    get address():ConsumerPostalAddress {
        return this._address;
    }

    get phoneNumber():string{
        return this._phoneNumber;
    }

    get personEmail():string{
        return this._personEmail;
    }

    get sellDate():string{
        return this._sellDate;
    }

    get invoiceNumber():string{
        return this._invoiceNumber;
    }

    get partnerRepUserId():string{
        return this._partnerRepUserId;
    }

    get partnerAccountId():string{
        return this._partnerAccountId;
    }

    get invoiceUrl():string{
        return this._invoiceUrl;
    }

    get isSubmitted():boolean{
        return this._isSubmitted;
    }

    get simpleLineItems():ConsumerSaleRegDraftSaleSimpleLineItem[]{
        return this._simpleLineItems;
    }

    get compositeLineItems():ConsumerSaleRegDraftSaleCompositeLineItem[]{
        return this._compositeLineItems;
    }

    get createDate():string{
        return this._createDate;
    }

    toJSON() {
        return {
            id: this._id,
            firstName: this._firstName,
            lastName: this._lastName,
            address: this._address,
            phoneNumber: this._phoneNumber,
            personEmail: this._personEmail,
            sellDate: this._sellDate,
            invoiceNumber: this._invoiceNumber,
            partnerRepUserId: this._partnerRepUserId,
            partnerAccountId: this._partnerAccountId,
            invoiceUrl: this._invoiceUrl,
            isSubmitted: this._isSubmitted,
            simpleLineItems: this._simpleLineItems,
            compositeLineItems: this._compositeLineItems,
            createDate: this._createDate
        }
    }

}

