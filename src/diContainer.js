import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ConsumerSaleRegistrationServiceSdkConfig from './consumerSaleRegistrationServiceSdkConfig';
import AddConsumerSaleRegDraftFeature from './addConsumerSaleRegDraftFeature';
import GetConsumerSaleRegDraftWithIdFeature from './getConsumerSaleRegDraftWithIdFeature';
import GetConsumerSaleRegDraftWithAccountIdFeature from './getConsumerSaleRegDraftWithAccountIdFeature';
import UpdateConsumerSaleRegDraftFeature from './updateConsumerSaleRegDraftFeature';
import DeleteConsumerSaleRegDraftWithIdFeature from './deleteConsumerSaleRegDraftWithIdFeature';
import UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature from './updateSaleInvoiceUrlOfConsumerSaleRegDraftFeature';
import RemoveSaleLineItemFromConsumerSaleRegDraftFeature from './removeSaleLineItemFromConsumerSaleRegDraftFeature';
import SubmitConsumerSaleRegDraftFeature from './submitConsumerSaleRegDraftFeature';
import IsEligibleForSpiffFeature from './isEligibleForSpiffFeature';
import GetConsumerSaleDraftExcludeSerialNumberWithDraftId from './getConsumerSaleDraftExcludeSerialNumberWithDraftId';

/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {ConsumerSaleRegistrationServiceSdkConfig} config
     */
    constructor(config:ConsumerSaleRegistrationServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(ConsumerSaleRegistrationServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {
        this._container.autoRegister(AddConsumerSaleRegDraftFeature);
        this._container.autoRegister(GetConsumerSaleRegDraftWithIdFeature);
        this._container.autoRegister(GetConsumerSaleRegDraftWithAccountIdFeature);
        this._container.autoRegister(UpdateConsumerSaleRegDraftFeature);
        this._container.autoRegister(DeleteConsumerSaleRegDraftWithIdFeature);
        this._container.autoRegister(UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature);
        this._container.autoRegister(RemoveSaleLineItemFromConsumerSaleRegDraftFeature);
        this._container.autoRegister(SubmitConsumerSaleRegDraftFeature);
        this._container.autoRegister(IsEligibleForSpiffFeature);
        this._container.autoRegister(GetConsumerSaleDraftExcludeSerialNumberWithDraftId);

    }

}
