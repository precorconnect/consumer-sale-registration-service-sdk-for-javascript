/**
 * @class {UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq}
 */
export default class UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq{

    _id:number;

    _invoiceUrl:string;


    constructor(id:number,
                invoiceUrl:string
    ){

        /**
         * getter methods
         */
        if(!id){
            throw new TypeError('id required');
        }
        this._id = id;

        if(!invoiceUrl){
            throw new TypeError('invoiceUrl required');
        }

        this._invoiceUrl = invoiceUrl;

    }

    /**
     * getter methods
     */
    get id():number{
        return this._id;
    }

    get invoiceUrl():string{
        return this._invoiceUrl;
    }

    toJSON() {
        return {
            id:this._id,
            invoiceUrl: this._invoiceUrl
        }
    }
}

