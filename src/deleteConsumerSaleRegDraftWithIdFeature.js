import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ConsumerSaleRegistrationServiceSdkConfig from './consumerSaleRegistrationServiceSdkConfig';

@inject(ConsumerSaleRegistrationServiceSdkConfig, HttpClient)
class DeleteConsumerSaleRegDraftWithIdFeature {

    _config:ConsumerSaleRegistrationServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ConsumerSaleRegistrationServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     *
     * @param consumerSaleRegDraftId
     * @param accessToken
     */
    execute(consumerSaleRegDraftId:number,
            accessToken:string){

        return this._httpClient
            .createRequest(`consumer-sale-registration/deleteConsumerSaleRegDraft/${consumerSaleRegDraftId}`)
            .asDelete()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()

    }
}

export default DeleteConsumerSaleRegDraftWithIdFeature;

