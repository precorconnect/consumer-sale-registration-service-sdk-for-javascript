import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ConsumerSaleRegSynopsisView from './consumerSaleRegSynopsisView';
import SubmitConsumerSaleRegDraftDto from './submitConsumerSaleRegDraftDto';
import ConsumerSaleRegistrationServiceSdkConfig from './consumerSaleRegistrationServiceSdkConfig';

@inject(ConsumerSaleRegistrationServiceSdkConfig, HttpClient)
class SubmitConsumerSaleRegDraftFeature {

    _config:ConsumerSaleRegistrationServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ConsumerSaleRegistrationServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     *
     * @param {SubmitConsumerSaleRegDraftDto} request
     * @param {string} accessToken
     * @returns {Promise.<ConsumerSaleRegSynopsisView>}
     */
    execute(request:SubmitConsumerSaleRegDraftDto,
            accessToken:string):Promise<ConsumerSaleRegSynopsisView> {

        return this._httpClient
            .createRequest(`consumer-sale-registration/submitconsumersaleregdraft`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);
    }
}

export default SubmitConsumerSaleRegDraftFeature;

