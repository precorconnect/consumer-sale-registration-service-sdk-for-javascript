/**
 * @class {ConsumerSaleRegDraftSaleSimpleLineItem}
 */
export default class ConsumerSaleRegDraftSaleSimpleLineItem{

    _id:number;

    _assetId:string;

    _serialNumber:string;

    _productLineId:number;

    _price:number;

    _productLineName:string;

    _productGroupId:number;

    _productGroupName:string;

    _productName:string;

    /**
     *
     * @param id
     * @param assetId
     * @param serialNumber
     * @param productLineId
     * @param price
     * @param productLineName
     * @param productGroupId
     * @param productGroupName
     * @param productName
     */
    constructor(id:number,
                assetId:string,
                serialNumber:string,
                productLineId:number,
                price:number,
                productLineName:string,
                productGroupId:number,
                productGroupName:string,
                productName:string
    ){
        if(!id){
            throw new TypeError('id required');
        }
        this._id = id;

        if(!assetId){
            throw new TypeError('assetId required');
        }
        this._assetId = assetId;

        if(!serialNumber){
            throw new TypeError('serialNumber required');
        }
        this._serialNumber = serialNumber;

        if(!productLineId){
            throw new TypeError('productLineId required');
        }
        this._productLineId = productLineId;

        this._price = price;

        if(!productLineName){
            throw new TypeError('productLineName required');
        }
        this._productLineName = productLineName;

        this._productGroupId = productGroupId;

        this._productGroupName = productGroupName;

        this._productName = productName;

    }

    /**
     * getter methods
     */
    get id():number{
        return this._id;
    }

    get assetId():string{
        return this._assetId;
    }

    get serialNumber():string{
        return this._serialNumber;
    }

    get productLineId():number{
        return this._productLineId;
    }

    get price():number{
        return this._price;
    }

    get productGroupId():number{
        return this._productGroupId;
    }

    get productGroupName():string{
        return this._productGroupName;
    }

    get productName():string{
        return this._productName;
    }

    toJSON(){
        return {
            id:this._id,
            assetId:this._assetId,
            serialNumber:this._serialNumber,
            productLineId:this._productLineId,
            price:this._price,
            productLineName:this._productLineName,
            productGroupId:this._productGroupId,
            productGroupName:this._productGroupName,
            productName:this._productName
        }
    }
}
