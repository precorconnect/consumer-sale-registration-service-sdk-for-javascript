import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ConsumerSaleRegSynopsisView from './consumerSaleRegSynopsisView';
import ConsumerSaleRegistrationServiceSdkConfig from './consumerSaleRegistrationServiceSdkConfig';

@inject(ConsumerSaleRegistrationServiceSdkConfig, HttpClient)
class GetConsumerSaleRegDraftWithIdFeature {

    _config:ConsumerSaleRegistrationServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ConsumerSaleRegistrationServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    execute(consumerSaleRegDraftId:number,
            accessToken:string):Promise<ConsumerSaleRegSynopsisView> {

        return this._httpClient
            .createRequest(`consumer-sale-registration/draftId/${consumerSaleRegDraftId}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => response.content);
    }
}

export default GetConsumerSaleRegDraftWithIdFeature;
