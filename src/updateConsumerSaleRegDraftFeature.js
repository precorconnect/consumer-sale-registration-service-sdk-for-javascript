import {inject} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import ConsumerSaleRegistrationServiceSdkConfig from './consumerSaleRegistrationServiceSdkConfig';
import UpdateConsumerSaleRegDraftReq from './updateConsumerSaleRegDraftReq';
import ConsumerSaleRegSynopsisView from './consumerSaleRegSynopsisView';

@inject(ConsumerSaleRegistrationServiceSdkConfig, HttpClient)
class UpdateConsumerSaleRegDraftFeature {

    _config:ConsumerSaleRegistrationServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:ConsumerSaleRegistrationServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     *
     * @param request
     * @param consumerSaleRegDraftId
     * @param accessToken
     * @returns {Promise|Promise.<T>}
     */
    execute(request:ConsumerSaleRegistrationServiceSdkConfig,
            consumerSaleRegDraftId:number,
            accessToken:string):Promise<ConsumerSaleRegSynopsisView> {

        return this._httpClient
            .createRequest(`consumer-sale-registration/updateconsumersaleregdraft/${consumerSaleRegDraftId}`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);

    }
}

export default UpdateConsumerSaleRegDraftFeature;
