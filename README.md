## Description
Precor Connect consumer sale registration service SDK for javascript.


## Setup

**install via jspm**  
```shell
jspm install consumer-sale-registration-service-sdk=bitbucket:precorconnect/consumer-sale-registration-service-sdk-for-javascript
```

**import & instantiate**
```javascript
import ConsumerSaleRegistrationServiceSdk,{ConsumerSaleRegistrationServiceSdkConfig} from 'consumer-sale-registration-service-sdk'

const consumerSaleRegistrationServiceSdkConfig =
    new ConsumerSaleRegistrationServiceSdkConfig(
        "https://api-dev.precorconnect.com"
    );
    
const consumerSaleRegistrationServiceSdk =
    new ConsumerSaleRegistrationServiceSdk(
        consumerSaleRegistrationServiceSdkConfig
    );
```

## Platform Support

This library can be used in the **browser**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```