/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
import ConsumerPostalAddress from '../src/consumerPostalAddress';

const dummy = {
    id:"1",
    firstName: 'firstName',
    lastName: 'lastName',
    addressLine1: 'addressLine1',
    addressLine2: 'addressLine2',
    city: 'city',
    iso31662Code: 'WA',
    postalCode: 'postalCode',
    iso31661Alpha2Code: 'US',
    phoneNumber: '123131233',
    personEmail: 'test@test.com',
    partnerAccountId:"001A000000MGmjvIAD",
    sellDate:'11/20/2015',
    installDate:'4/6/2015',
    invoiceNumber:"123456",
    partnerRepUserId:'00u5fj21jtj63phiA0h7',
    invoiceUrl:"url",
    isSubmitted:false,
    createDate:'4/6/2015',
    url: 'https://dummy-url.com',
    submittedByName:"Test User",
    simpleLineItemId:2,
    assetId:"123",
    serialNumber:"123456",
    productLineId:340,
    price:10,
    productLineName:"line1",
    productGroupId:300,
    productGroupName:"group1",
    productName:'productName'
};

dummy.address =
    new ConsumerPostalAddress(
        dummy.addressLine1,
        dummy.addressLine2,
        dummy.city,
        dummy.iso31662Code,
        dummy.postalCode,
        dummy.iso31661Alpha2Code
    );

export default dummy;


